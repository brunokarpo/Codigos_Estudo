package consumidor;

import produtos.Boleto;
import produtos.NFe;
import abstractFactory.ModuloVendaFactory;

public class Venda {

	private ModuloVendaFactory factory;

	public Venda(ModuloVendaFactory factory) {
		this.factory = factory;
	}

	public void realizar() {
		System.out.println("Realizando uma venda. Emitindo NFe");
		NFe nota = factory.gerarNFe();
		nota.gerar();

		System.out.println("Imprimindo Boleto");
		Boleto boleto = factory.gerarBoleto();
		boleto.emitir();
	}

}
