package abstractFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

class PropertiesManipulador {

	Properties getProperties() throws IOException {
		Properties prop = new Properties();

		File arquivo = new File("./resources/config-docs.properties");
		InputStream is = new FileInputStream(arquivo);

		prop.load( is );

		return prop;
	}

}
