package abstractFactory;

import java.io.IOException;
import java.util.Properties;

import produtos.Boleto;
import produtos.NFe;

public class ModuloVendaImp implements ModuloVendaFactory {

	private Properties props;

	public ModuloVendaImp() throws IOException {
		PropertiesManipulador manipulador = new PropertiesManipulador();
		props = manipulador.getProperties();
	}

	@Override
	public NFe gerarNFe() {
		String classenfe = props.getProperty("nfe");
		NFe nota = null;
		try {
			nota = (NFe) Class.forName(classenfe).newInstance();
		} catch (InstantiationException | IllegalAccessException
				| ClassNotFoundException e) {
			System.err.println("Problema ao gerar a implementacao da NFe");
		}
		return nota;
	}

	@Override
	public Boleto gerarBoleto() {
		Boleto boleto = null;
		String classeBoleto = props.getProperty("boleta");

		try {
			boleto = (Boleto) Class.forName(classeBoleto).newInstance();
		} catch (InstantiationException | IllegalAccessException
				| ClassNotFoundException e) {
			System.err.println("Problema ao gerar a implementacao do Boleto");
		}

		return boleto;
	}

}
