package abstractFactory;

import produtos.Boleto;
import produtos.NFe;

public interface ModuloVendaFactory {

	public NFe gerarNFe();

	public Boleto gerarBoleto();

}
