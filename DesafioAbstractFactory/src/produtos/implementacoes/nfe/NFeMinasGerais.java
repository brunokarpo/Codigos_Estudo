package produtos.implementacoes.nfe;

import produtos.NFe;

public class NFeMinasGerais implements NFe {

	@Override
	public void gerar() {
		System.out.println("Quando estamos no ar autorizamos uma NF-e com imposto de 15%");
	}

}
