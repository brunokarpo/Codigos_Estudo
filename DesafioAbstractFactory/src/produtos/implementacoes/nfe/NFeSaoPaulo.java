package produtos.implementacoes.nfe;

import produtos.NFe;

public class NFeSaoPaulo implements NFe {

	@Override
	public void gerar() {
		System.out.println("Gerando uma NF-e para o estado de São Paulo com o imposto de 10%");
	}

}
