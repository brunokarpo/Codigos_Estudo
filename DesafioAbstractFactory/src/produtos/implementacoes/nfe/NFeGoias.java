package produtos.implementacoes.nfe;

import produtos.NFe;

public class NFeGoias implements NFe {

	@Override
	public void gerar() {
		System.out.println("Gerando uma NF-e para o estado de Goiás com o imposto de 7%");
	}

}
