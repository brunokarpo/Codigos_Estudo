package produtos.implementacoes.boleta;

import produtos.Boleto;

public class BoletoCaixaEconomicaFederal implements Boleto {

	@Override
	public void emitir() {
		System.out.println("Emitindo um boleto com a Caixa Economica Federal");
	}

}
