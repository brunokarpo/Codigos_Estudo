package produtos.implementacoes.boleta;

import produtos.Boleto;

public class BoletoBancoBrasil implements Boleto {

	@Override
	public void emitir() {
		System.out.println("Emitindo um boleto pelo Banco do Brasil");
	}

}
