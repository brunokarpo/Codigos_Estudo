import java.io.IOException;

import abstractFactory.ModuloVendaImp;
import consumidor.Venda;


public class Principal {

	public static void main(String[] args) {
		try {

			Venda venda = new Venda(new ModuloVendaImp());
			venda.realizar();

		} catch (IOException e) {
			System.err.println("Problema para carregar o arquivo properties");
		}
	}

}
