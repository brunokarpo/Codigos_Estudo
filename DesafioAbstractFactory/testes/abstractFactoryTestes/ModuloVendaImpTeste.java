package abstractFactoryTestes;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import produtos.Boleto;
import produtos.NFe;
import abstractFactory.ModuloVendaFactory;
import abstractFactory.ModuloVendaImp;

public class ModuloVendaImpTeste {

	private ModuloVendaFactory abstractFactory;

	@Before
	public void criarAbstractFactory() {
		try {
			abstractFactory = new ModuloVendaImp();
		} catch (IOException e) {
			System.err.println("Nao conseguiu capturar o arquivo properties");
			fail();
		}
	}

	@Test
	public void validarCapturaCorretaDasImplementacoes() {
		NFe nota = abstractFactory.gerarNFe();
		assertNotNull(nota);

		Boleto boleto = abstractFactory.gerarBoleto();
		assertNotNull(boleto);
	}

}
