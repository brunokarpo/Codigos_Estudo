package operacoes;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CalculadoraTeste {

	private ICalculadora calculadora;

	@Before
	public void criarCalculadora() {
		calculadora = new CalculadoraProxy();
	}

	@Test
	public void deveConseguirSomar() {
		assertEquals(32, calculadora.somar(16, 16), 0.00001);
		assertEquals(37.18, calculadora.somar(20.28, 16.90), 0.00001);
		assertEquals(-57, calculadora.somar(-100, 43), 0.00001);
	}

	@Test
	public void deveConseguirSubtrair() {
		assertEquals(0, calculadora.subtrair(16, 16), 0.00001);
		assertEquals(3.38, calculadora.subtrair(20.28, 16.90), 0.00001);
		assertEquals(-143, calculadora.subtrair(-100, 43), 0.00001);
	}

	@Test
	public void deveConseguirMultiplicar() {
		assertEquals(256, calculadora.multiplicar(16, 16), 0.00001);
		assertEquals(342.732, calculadora.multiplicar(20.28, 16.90), 0.00001);
		assertEquals(-4300, calculadora.multiplicar(-100, 43), 0.00001);
	}

	@Test
	public void deveConseguirDividir() {
		assertEquals(1, calculadora.dividir(16, 16), 0.00001);
		assertEquals(1.2, calculadora.dividir(20.28, 16.90), 0.00001);
		assertEquals(-2.325581395, calculadora.dividir(-100, 43), 0.00001);
	}

	@Test
	public void divisaoPorZeroNaoDeveApresentarProblemas() {
		assertEquals(0, calculadora.dividir(150, 0), 0.00001);
	}
 }
