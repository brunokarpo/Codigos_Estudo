package operacoes;

public class CalculadoraProxy implements ICalculadora{

	private ICalculadora calculadora;

	public CalculadoraProxy() {
		calculadora = new CalculadoraImpl();
	}

	/**
	 * @param a
	 * @param b
	 * @return
	 * @see operacoes.ICalculadora#somar(double, double)
	 */
	public double somar(double a, double b) {
		return calculadora.somar(a, b);
	}

	/**
	 * @param a
	 * @param b
	 * @return
	 * @see operacoes.ICalculadora#subtrair(double, double)
	 */
	public double subtrair(double a, double b) {
		return calculadora.subtrair(a, b);
	}

	/**
	 * @param a
	 * @param b
	 * @return
	 * @see operacoes.ICalculadora#multiplicar(double, double)
	 */
	public double multiplicar(double a, double b) {
		return calculadora.multiplicar(a, b);
	}

	/**
	 * @param a
	 * @param b
	 * @return
	 * @see operacoes.ICalculadora#dividir(double, double)
	 */
	public double dividir(double a, double b) {
		if(b == 0) {
			return 0;
		}
		return calculadora.dividir(a, b);
	}

}
