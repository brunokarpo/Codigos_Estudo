package operacoes;

public interface ICalculadora {
	public double somar(double a, double b);
	public double subtrair(double a, double b);
	public double multiplicar(double a, double b);
	public double dividir(double a, double b);
}
