Topicos importantes de estudo JSF
======

### Framework Action Based x Component Based
- Action based: você escreve classes que representam ações. Está mais perto do protocolo HTTP. (Servlets puros)
- Component based: os elementos da página são representados por componentes no servidor (Classes JAVA). Elas escondem os detalhes do protocolo HTTP; Bem parecido com o desenvolvimento desktop.

### JAVA EE
- Roda sobre a plataforma JAVA SE.
- Simplifica desenvolvimento fornecendo funcionalidades comuns em aplicações corporativas.
- Não é um produto e sim uma especificação;
- Especificação guarda-chuva. Serve de base para outras especificações.
- http://jcp.org/ Página da Java Community Process. https://jcp.org/en/jsr/detail?id=366 (JAVA EE 8)

### Implementações JSF
- JSF não é um produto e sim uma especificação que atende a especificação JAVA EE.
- A especificação precisa ser implementada. Existe uma implementação de referência. Para o JSF a especificação padrão é o Mojarra.
- http://javaserverfaces.java.net/ Aqui você baixa a implementação Mojarra.

### Implementação JSF x Biblioteca de componentes
- JSF é uma especificação.
- Existem implementações da especificação JSF, tal como Mojarra, MyFaces dentre outras;
- **Mojarra** é a implementação de referência (RI). É a implementação que valida se a especificação funciona. Compatível 100% com a especificação JSF.
- As especificações se diferenciam em vários detalhes, como suporte, estrutura dos componentes (estética). Existe também diferenças na forma como o código foi escrito (qualidade do código) e algumas implementações trazem customizações específicas.
- Conceitualmente se você segue a especificações do JSF você pode migrar entre as especificações que a aplicação deve continuar funcionando. Na prática pode existir incompatibilidades devido a funcionalidades específicas de uma implementação.
- O Mojarra é baixado no seguinte link http://javaserverfaces.java.net/;
- Biblioteca de componentes são biblioteca de terceiros que fornecem alguns componentes mais elaborados para uma página desenvolvida em JSF.
- As bibliotecas de terceiros utilizam uma implementação da especificação JSF para criar seus componentes personalizados, exemplo: calendários!
- Exemplo de biblioteca de terceiros: PrimeFaces, RichFaces, etc.

### O arquivo WEB.xml
O arquivo web.xml é um arquivo que sempre estará presente em aplicações JAVA WEB. Ele especifica alguns comportamentos da aplicação Web além de algumas configurações para que a aplicação funcione.
Arquivo de configuração Web em JAVA.

### Managed beans
- Serve como um canal entre a interface gráfica e o back-end da aplicação.
- No managed bean está a implementação do comportamento da tela desenvolvida em JSF;
- Ele deve servir como um controlador. Nele não está implementado regras de negócio, apenas chamadas para a camada de negócio.
- As classes managed beans precisam estar anotadas na definicação da classe com a anotação @ManagedBean
- Deve sempre ter um construtor padrão (sem argumentos);
- Por ser um JAVA BEAN ele deve ter métodos Getters e Setters para seus atríbutos.

### Backing beans
- É um tipo especial de Managed Bean;
- Faz acesso direto aos componentes da página, permitindo alterar alguns atributos desses componentes diretamente.
- O segredo do Backing Bean é a declaração dos atríbutos da página no código JAVA em um Managed Bean e a ligação entre o atríbuto da página xhtml com o código JAVA através do elemento Binding.
- O uso de Backing Bean não é aconselhado pois ele torna o código complexo de manter. As mesmas ações providas por um backing bean estão disponíveis na página de visualização do JSF através dos atríbutos de componentes.

### Escopo de um Managed Bean
Um managed bean possui um escopo. Escopo é entendido como o tempo de vida de um Managed Bean.
Vamos falar sobre dois escopos do JSF:

- Aplicação: o escopo de aplicação é um escopo que compartilha os mesmos dados de um managed bean com todos os usuários do sistema. É providenciado pela anotação @ApplicationScoped do pacote javax.faces.bean
- Sessão: o escopo de sessão é um escopo que mantem os dados de um managed bean para o usuário específico que está utilizando aquela página. Se dois usuário estiverem utilizando a mesma página, cada um terá sua sessão própria. É providenciado pela anotação @SessionScoped do pacote javax.faces.bean;
- View: o escopo de visualização é um escopo menor do que o escopo de sessão. O Managed Bean permanece ativo enquanto o usuário estiver utilizando uma tela específica. Quando a tela é modificada ou recarregada o Managed Bean é reiniciado. É providenciado pela anotação @ViewScoped do pacote javax.faces.bean;
- Requisição: é o escopo padrão de todo ManagedBean que não está anotado com nenhum escopo; Pode ser providenciado pela anotação @RequestScoped. O tempo de vida dele é curto, iniciando a cada requisição e morrendo ao final dessa.
- Sem Escopo: é o escopo mais curto. Qualquer managed bean anotado com @NoneScoped é iniciado para cada componente de uma tela, morrendo após a finalização da instanciação. É o escopo menos usado;
#### Dica
Os beans envolvidos no JSF precisam ser serializados. Implements Serializable.

#### Anotações @PostConstruct:  
Essa anotação é utilizada sobre os métodos que você precisa que sejam chamados imediatamente após a criação de um determinado Managed Bean. Serve para carregar recursos, se necessários

#### Anotação @PreDestroy:  
Essa anotação é utilizada sobre os métodos que você precisa que sejam chamados imediatamente antes da finalização de um Managed Bean. Serve apra fechar recursos que estejam sendo utilizados, quando necessário.

### Navegação Implicita
Navegação implicita é quando as páginas são linkadas umas as outras para o usuário conseguir navegar pela aplicação.  
No JSF esse tipo de linkagem pode ocorrer utilizando os componentes `<h:commandLink>` ou `<h:commandButton>`. Dentro desses commandos você passa o parâmetro action que deve apontar para a página de destino sem colocar a extensão da mesma. Por exemplo:  

    <h:commandLink action="OutraPagina" />

  
É importante atentar que colocando da forma como foi especificado acima, a página será reinderizada na mesma requisição JSF, ou seja, o conteúdo será modificado no browser, porém o endereço da página WEB permanecerá o mesmo da página 'anterior'. Isso porque só ocorre uma requisição / resposta, sem redirecionamento de páginas.  
Para que o redirecionamento funcione corretamente é necessário utilizar o complemento "?faces-redirect=true" ao outcome do action, dessa forma:  

    <h:commandLink action="OutraPagina?faces-redirect=true" />

Assim a resposta será realmente redirecionada. O problema é que o redirecionamento ocorre em duas requisições JSF, ou seja, quando o usuário clica no botao/link ocorre uma requisição para o servidor que retorna como resposta o status HTML 302 (movido temporariamente). Esse status retorna com o link da nova página, o que é tratado em uma nova requisição JSF. O problema disso é que se tiver dados de um Managed Bean que está em RequestScoped no momento do clique no botão, esses dados serão perdidos devido ao escopo do ManagedBean.  

### Manipulando eventos de ação;
- actionListener: Resumidamente são ouvidores de ações disparados por botões ou links no JSF. Sua função é ser executado antes do atríbuto action para fazer algum registro, ou executar alguma ação que precisa ser executada antes da execução de uma determinada ação.
Os métodos chamados no ActionListener precisam receber como parâmetro um objeto do tipo ActionEvent. Ele pode fazer a manipulação do evento e executar as ações necessárias antes da execução de uma determinada ação;

É possível criar classes que generalizam algumas ações, para elas serem extensíveis para outras páginas JSF. Essas classes são Managed Bean que implementam a interface ActionListener do pacote javax.faces.event; Ao implementar essa interface, é necessário sobrescrever o método processAction. 

Após a criação dessa classe, é necessário importar no xhtml o XMLNS http://java.sun.com/jsf/core. Ele provê alguns componentes do núcleo do JSF. Utilizando o componente `<f:actionListener>` e amarrando ele à classe que implementa ActionEvent o listener é sempre chamado antes da execução de alguma ação no JSF.

### Manipulando eventos de mudança de valor
Eventos de mudança de valores são listeners que podem ser chamados quando o valor de algum componente muda. O método que implementa a ação desse listener deve receber como parâmetro um objeto do tipo ValueChangeEvent do pacote javax.faces.event;
Na página de visualização do JSF é chamado o atríbuto valueChangeListener referenciando o método criado no manangedbean que recebe o parametro ValueChangeEvent. Esse listener é chamado na submissão da requisição e é executado antes da ação proposta na submissão.

### Ciclo de vida do JSF:
1. Restaurar Visão: nessa fase é recuperado a arvore de componentes JSF com a hierarquia dos componenentes que compoe a tela.
2. Aplicar valores da requisição: nessa fase cada componente é responsável por pegar os valores de interesse dele. Exemplo: um inputText vai pegar o texto digitado no seu campo. É importante que se saiba que esses valores de requisição são sempre em formato String e são os valores oriundos dos componentes HTML.
3. Processar requisição: nessa fase os valores recuperados na fase anterior são convertido para os tipos de dados JAVA para serem processados. Também as validações sobre os dados são feitas nessa fase para garantir que os dados recuperados estão de acordo com o que o servidor precisa para resolver a requisição proposta.
4. Atualizar os valores do modelo: nessa fase o JSF pega os dados processados da requisição e atribui aos Beans referente a ele, através dos métodos Getters e Setters desses Beans.
5. Invocar a aplicação: nessa fase são executados os actionsListeners e actions que gerou a chamada da requisição da página JSF.
6. Reinderizar a resposta: a resposta da requisição é convertida para HTML e mostrada no browser do cliente.

#### Diferença entre Primeira Requisição x Postback:  
Primeira requisição é quando o cliente chama a página do JSF pela primeira vez, seja através da URL ou navegação implicita. Nessa fase apenas a fase 1 e 6 do ciclo de vida do JSF é executado.
Postback ocorre quando o usuário já está em uma página JSF e executa alguma ação que faz submissão de informações em busca de resposta. Nesse caso todo o ciclo de vida do JSF é executado.


Principais componentes JSF
-------------------
Principais componentes JSF são os componentes básicos que vem na implementação padrão do JSF. São os componentes básicos. Existem componentes customizados de terceiros, mas aqui focaremos nos componentes base;

### Atributos comuns dos componentes JSF.
* Atributos pastTrue: São atributos de componentes exatamente iguais ao HTML. Ele simplesmente reenderiza no HTML;
	* *size*: Define o tamanho de um componente.
	* *maxlength*: tamanho máximo do conteúdo que um componente pode ter no JSF.
	* *title*: título de um componente; Texto que aparece no componente quando ele está apontado pela seta do mouse.
	* *id*: O id é um componente importante pois ele é o identificador do componente e sua atribuição facilita a manipulação dos campos na visualização do JSF.
	* *style*: aplica estilo CSS inline para o campo onde o atributo foi declarado;
	* *styleClass*: aplica estilo CSS utilizando o atributo class do CSS da página. 
	* *rendered*: utilizado para quando o componente precisa ser renderizado em situações específicas. Faz sumir o componente quando necessário;
	* Existem outros vários, mas esses são alguns exemplos.
* Atributos de HTML;
	* *onfocus*: ação que é executada quando um elemento recebe o foco na tela. Geralmente roda um JavaScript;
	* *onclick* : executa ação, geralmente JavaScript quando um botão é clicado;

### Entrada e saídas de texto e imagens;
Existem 3 componentes básicos de saída de texto:
* **outputText**: componente que simplesmente reenderiza um texto na interface Web. Ele é utilizado para gerenciar um componente no JSF.
* **outputFormat**: semelhante ao outputText, o outputFormat também é um componente de saída de texto, porém ele recebe parâmetro através do componente `<f:param>` do pacote disponível em http://java.sun.com/jsf/core;
* **outputLabel**: outro componente de saída de texto, dessa vez utilizado para servir como label de formulários no HTML. Através do parâmetro `for` podemos clicar no label e ele dá o foco no campo de texto que ele está atribuído. Ele recebe como argumento o ID do campo que ele tem de dar o foco;

Agora os componentes de entrada de texto são:
* **inputText** campo de texto utilizado para entrada de textos diversos. É reenderizado em apenas uma linha;
* **inputSecret**: componente que serve para entrada de texto secretos, por exemplo, campos para digitação de senhas;
* **inputTextarea**: é um campo de entrada de texto. Ele vem com mais de uma linha; através da utilização dos atríbutos `cols` é possível definir a quantidade de colunas que o campo de texto terá e o atríbuto `rows` define quantas linhas o campo terá;

O componente de imagem é o `<h:graphicImage>`. Ele recebe como value o caminho da imagem dentro do projeto, sem precisar passar o nome do contexto. Ele reenderiza no HTML inserindo o nome do contexto, o que abstrai o desenvolver desse detalhe e evita falhas na hora de renderizar o componente;

### Menus, caixa de listagem e itens de seleção
* **selectOneMenu** é um menu que serve para selecionar somente um elemento; O value dele é o set do alvo da caixa de seleção; Os itens do menu são inseridos através do componente `<f:selectItem>` do pacote http://java.sun.com/jsf/core. Nele vai os parâmetros `itemValue` que infroma o valor do item. Também pode ir o parâmetro `itemLabel` que é a label referente ao item (somente para visualização); Quando o *itemLabel* não aparece, o componente assume como Label o *itemValue*;
* **selectOneListbox**: é um menu que serve para selecionar somente um elemento, porém ele é reenderizado em uma caixa de listagem que aparece vários itens; Dentro dele, o parâmetro `size` define a quantidade de itens que aparece na caixa de seleção;
* **selectManyMenu**: é uma caixa de seleção na qual o usuário pode selecionar vários itens simultâneos; Em geral é renderizado em uma linha;
* **selectManyListbox**: é uma caixa de seleção na qual o usuário pode selecionar vários itens simultâneos e é renderizado em várias linhas. Também utiliza o atríbuto `size`;

### Campos de checagem e botões radio
* **selectOneRadio**: cria um componente com vários itens onde somente um pode ser acionado. Renderiza o radioButton do HTML; Ele pode receber o parâmetro `layout` que define qual o layout que os componentes serão apresentados. Recebe como argumentos *lineDirection* para apresentar as opções em linha e *pageDirection* para apresentar as opções em lista vertical;
* **selectBooleanCheckbox**: componente de validação que retorna simplesmente um valor boleano (*true* ou *false*);
* **selectManyCheckbox**: componente que disponibiliza vários selectButtons que podem serem selecionados simultaneamente;


### Botões e Links
* **commandButton**: Botão que executa alguma ação em um formulário da página; Existe o atríbuto `type` que ao receber o valor "reset" é usado para limpar os dados de um formulário; Também ele pode receber o atríbuto `image` que recebe como parâmetro o endereço de uma imagem que será reenderizada como botão na página.

* **commandLink**: é um link que executa um comando. Basicamente é a mesma forma de utilizar o commandButton. Porém, ao invés de criar um botão, ele reenderiza um texto de link; Ele não é a ação padrão de um formulário, portanto ele precisa ser clicado para executar. Também é possível colocar uma imagem como link no commandLink utilizando o componente `<f:graphicImage>`
* **outputLink**: É um link que engloba um texto que é geralmente utilizado para direcionar para páginas externas;
* **link**: É semelhante ao outputLink, porém o seu atríbuto `value` recebe como parâmetro o texto que será renderizado. O direcionamento ocorre através do atríbuto `outcome`; Outra caracteristica importante é que ele não submete os dados ao serem clicados;
* **button**: gera um botão na tela que não é do tipo submit, ou seja, não é chamado por padrão ao clicar em enter. Ele deve receber um atríbuto chamado `outcome` que faz o direcionamento para outra tela, seguindo o princípio da navegação implicita;
### Paineis
* **panelGrid**: componente utilizado para gerenciar layout em formato de grid (tabelas); Para definir a quantidade de colunas no Grid usasse o atríbuto `columns` que recebe como parâmetro a quantidade de colunas;
* **facet**: componente que gerencia os campos de uma tabela. Usa-se o atríbuto `name` para definir ao que o facet se refere, exemplo: "header" (cabeçalho de uma tabela);
* **panelGroup**: componente utilizado para colocar vários componentes dentro de um mesmo campo do grid. Agrupa componentes filhos;


### Mensagens
* **FacesContext**: é um objeto que contém cada estado de cada requisição relacionado ao processamento e reenderização da resposta. Ele tem uma fila de mensagens que são utilizadas pelo Painel. Para adicionar mensagem na fila usa-se o método addMessage que recebe um clientId e a mensagem;
* **FacesMessage**: Objeto que contém a mensagem do JFS.
* **messages**: esse componente exibe todas as mensagens que foram adicionadas na fila do contexto JSF. Por padrão ele limpa as mensagens da Fila após exibí-las. Também contém o atríbuto `layout` que por padrão é 'list', mas pode receber outros formatos de layout, tal como 'table'; Ele reenderiza as mensagens no layout especificado. Também pode customizar o estilo da mensagem de acordo com sua serveridade utilizando CSS através dos atríbutos `errorStyle`, `infoStyle`, `warnStyle`, `fatalStyle`, `errorClass`, `infoClass`, `warnClass`, `fatalClass`;
Também pode receber o atríbuto `showDetail` que recebe como parâmetro um booleano que ativa os detalhes da mensagem de erro; O atríbuto `showSummary` também valida a renderização do sumário do erro;
Também tem o atríbuto `globalOnly` que ativa somente a renderização de mensagens globais (não ligada a nenhum componente [clientId == null])	
* **message**: exibe a mensagem de erro próximo ao componente onde o erro ocorreu; recebe o atríbuto `for` que recebe como parâmetro o `id` do componente alvo da validação;


### Tabela de dados
* **dataTable**: renderiza uma tabela. Recebe um atributo chamado `value` que é conectado com a lista de dados a serem mostrados no ManagedBean. Cada cliente é atribuído a uma variável que é instanciada através do atríbuto `var`; As bordas são ativadas através do atríbuto `border`;
* **column**: define as colunas de uma tabela. Recebe componentes que serão apresentados nas linhas daquela coluna;
* **facet**: define o cabeçalho das colunas;
* **rendered**: define se o componente será renderizado se atender determinada expressão;

### Componentes dentro de células
Basicamente dá para usar qualquer elemento aprendido anteriormente dentro das celulas de uma tabela, basta definí-los;

### Aplicando estilos em tabelas:
* **styleClass**: atríbuto que é utilizado para atribuir uma classe de estilo CSS em algum componente JSF.
* **headerClass**: atríbuto do componente `<h:dataTable>` utilizado para definir a classe de estilo CSS que o cabeçalho da tabela terá;
* **rowClasses**: atríbuto do componente `<h:dataTable>` utilizado para aplicar estilos CSS intercalados em linhas de uma tabela (cor-sim, cor-não); As classes devem vir separadas por virgulas;
* **columnClasses**: basicamente faz a mesma coisa que rowClasses, porém aplica o estilo intercalado das colunas e não das linhas;

### Arquivos CSS e JavaScript
Quando queremos utilizar arquivos CSS e JavaScript com JSF, precisamos criar uma pasta chamada "resources" dentro da página de contexto web do Tomcat, por exemplo. Essa pasta passa a ser a 'biblioteca' de recursos web.
* **outputStylesheet**: componente JSF que carrega os arquivos CSS. A biblioteca é carregada através do atríbuto `library` que recebe como parâmetro o nome da pasta que está dentro da pasta 'resources' onde está o arquivo. O atríbuto `name` define o nome do arquivo onde está definido os estilos CSS;
* **outputScript**: componente JSF que carrega os arquivos JavaScript. Tem as mesmas caracteristicas de 'outputStylesheet', porém esse aqui é somente para JavaScript;


Conversores e validação
-----------------------

Toda requisição WEB transita dados sempre no formato String. O ciclo de vida do JSF ajuda o programador fornecendo conversores e validadores padrões para os tipos básicos conhecidos do JAVA, porém não é todos os tipos de dados que o JSF conhece como converter de String para Objeto e vice-versa. Para isso são utilizados os conversores e validadores;

### Conversores de números e datas
* **converterDateTime**: esse é um componente fornecido pela biblioteca http://java.sun.com/jsf/core que faz as conversões padrões dos tipos String para Date. Ele pode receber o atríbuto `pattern` que especifica o formato da data que é preciso receber / retornar para o usuário. Também aceita o atríbuto `locate` que recebe como parâmetro a linguagem que ele deve devolver o formato da hora;

* **convertNumber**: é um componente que possibilita a customização das conversões de números para Strings. Tem vários atributos tal como `type` que define o tipo de número [ex.: currency para moeadas], `currencySimbol` que define qual o simbolo utilizado para moedas, `locale` que define a linguagem / internacionalização dos números,  `minFractionalDigits` que define a quantidade mínima de casas decimais, `converter` que define qual conversor o JSF vai tentar utilizar entre tantos outros que podem ser vistos na documentação;

### Customizando mensagens de erro de conversão

....

### Validadores
Validadores são utilizados para proteger o modelo de dados da aplicação contra valores inconsistentes inseridos na View. Na WEB as informações são transitadas em formato String, logo, quando um formulário é submetido, o dado é convertido pelo JSF para o tipo esperado dele, utilizando os conversores. Porém esses valores, às vezes, precisa ser validado para atender uma faixa de valores determinados e não causar inconsistências no modelo de dados;
* **required**: Valida se um campo é requerido. atributo recebe um parâmetro booleano e por padrão é false. Quando inserido true, o campo obrigatoriamente deve ser preenchido antes da submissão de um formulário;
* **validadeLenght**: componente fornecido por http://java.sun.com/jsf/core que faz validação do tamanho das String inserida num componente. Essa validação só ocorre se existir algum valor preenchido no campo; Recebe o atríbuto `minimum` que valida o tamanho mínimo da String e `maximum` que valida o tamanho máximo de uma String;
* **validateLongRange**: componente fornecido por http://java.sun.com/jsf/core que valida o intervalo de valores inteiros permitido dentro de um componente de entrada; Recebe o atríbuto `minimum` que valida qual o menor valor permitido e `maximum`que valida qual o valor máximo permitido no campo;
* **validateDoubleRange**: componente fornecido por http://java.sun.com/jsf/core que valida o intervalo de valores de ponto flutuante permitido dentro de um componente de entrada; Recebe o atríbuto `minimum` que valida qual o menor valor permitido e `maximum`que valida qual o valor máximo permitido no campo;

### Customizando mensagens de erros de validação
* **requiredMessage**: atríbuto que customiza a mensagem de obrigatoriedade de um campo diretamente na view.

### Criando conversores personalizados

* **validatorMessage**: atributo que customiza a mensagem de validação de um componente de entrada diretamente na view;

Para criar um conversor personalizado é necessário criar uma classe JAVA que implementa a interface `Converter` do pacote javax.faces.converter. Ela contém dois métodos que precisam ser implementados: getAsObject e getAsString.
O método getAsObject recebe uma String como parâmetro oriunda da View do JSF e esse método o converte para o Objeto que o conversor deseja. O método getAsString recebe um objeto como parâmetro, geralmente oriundo do back-end da aplicação e o transforma em String para ser apresentado na camada de visualização.
A classe conversora criada precisa ser anotada com a anotação `@facesConverter` do pacote javax.faces.convert.FacesConverter. Essa anotação precisa receber um parâmetro String que é o apelido do conversor no front-end. Por padrão ele deve ser nomeado seguindo a estrutura de pacotes do Java, ou seja, o inverso de um domínio (ex.: nom.brunokarpo.MeuConverter

Na View do JSF o conversor é acionado através do atríbuto `converter` que recebe como parâmetro o apelido do conversor criado que se deseja usar;


### Cirando validadores personalizados.
Um validador personalizado é muito parecido com um conversor personalizado. Para criar um é necessário criar a classe JAVA que implementa a interface `Validator`. Ela contém o método `validade` que precisa ser implementado pois ele fará a validação do conteúdo que ele recebe. A classe também precisa receber a anotação `@facesValidator` que, tal como @facesConverter recebe como parâmetro uma String contendo um apelido e segue a mesma estrutura de criação (domínio ao contrário).

É importante salientar que o validador é executado depois do conversor, seguindo o princípio do ciclo de vida do JSF. Caso a validação falhe é preciso lançar a exceção `ValidadorException` que pode receber como parâmetro um FacesMessage contendo a mensagem do erro.

Na view do JSF é preciso adicionar o atríbuto `converter` no componente de entrada, passando como parâmetro o ID do validador. Porém essa forma tem a limitação de usar somente um validador para o campo. Em alguns casos é preciso utilizar mais de um validador no campo de entrada. Para isso é possível utilizar o componente `<f:validator>` uma ou várias vezes. Ele deve conter o atríbuto `validatorId` que recebe como parâmetro a String colocada como apelido na anotação @facesValidator da classe que irá fazer a validação;


Persistência de dados
---------------------

### Conhecendo e configurando o JPA 2 com Hibernate
JPA é uma especificação ORM (Object Relacional Mapping) que serve para fazer persistência de objetos JAVA em banco de dados Relacional
`Hibernate` implementação do JPA que usaremos;	

<< vamos deixar as caracteristicas desse assunto para um curso específico >>

JSF Avançado
------------

### Suporte ao método Get
Por padrão do JSF trabalha com o protocolo POST do HTTP, ou seja, qualquer dado submetido ao ManagedBean não terá seus atríbutos apresentados na URL. Dessa forma a URL não fica bagunçada nem os dados ficam expostos.

Porém é possível passar parâmetros para a URL para recuperar dados específicos do banco através da passagem da URL.

* **metadata** componente fornecido por http:/java.sun.com/jsf/core que captura metadados passados pela URL e o usa para a conversão dos metadados em objetos para a camada de visualização.
* **viewParam** visualizador de parâmetros dos Metadados que captura qual metadado é o objetivo da captura através do atríbuto `name` e o atribuí ao objeto declarado em `value`

### Regras de navegação Explicitas
Navegação explicitas, diferente da navegação implícita, facilita o processo de redirecionamento e navegação das páginas, tornando-as mais dinâmicas no código. Assim é fácil criar redirecionamentos ou mesmo renomear páginas sem perder toda a navegação já implementada, facilitando a manutenção do código;


### Usando AJAX no JSF.
AJAX é o uso de tecnologias WEB para tornar a página mais interativa, sem o reload para cada ação de submissão. O JSF 2 já tem suporte integrado com AJAX, sem a necessidade de nenhuma configuração adicional para usá-lo;
Os componentes que vão fazer o uso do AJAX precisam usar o componente `<f:ajax>`. Ele recebe 3 atríbutos basicamente:
* event: qual o evento JAVASCRIPT que vai acionar o AJAX. Ex.: 'keyup', 'click', etc; O event geralmente é o evento padrão do componente;
* render: recebe como parâmetro qual o ID do componente que será renderizado sem o refresh de tela. Pode receber também alguns parâmetros como @this, @all;
* execute: quando o componente AJAX for executado, solicita que o servidor processe os componentes marcados nesse atríbuto. O padrão é o `@this`, mas pode receber @all para a página inteira;

Em alguns momentos, alguns componentes já podem estar chamando eventos JAVASCRIPT para fazer alguma ação automatizada. Nesses casos o AJAX pode ser chamado via API JavaScript. Essa API está disponibilizada em `jsf.ajax.*`;

### Template de páginas com Facelets

