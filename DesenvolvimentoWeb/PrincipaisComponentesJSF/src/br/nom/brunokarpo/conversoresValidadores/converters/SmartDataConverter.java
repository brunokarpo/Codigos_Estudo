package br.nom.brunokarpo.conversoresValidadores.converters;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.faces.FacesException;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

@FacesConverter("nom.brunokarpo.SmartDataConverter")
public class SmartDataConverter implements Converter {

	private static SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");

	private Date getDataHoje() {
		Calendar hoje = Calendar.getInstance();
		hoje.set(Calendar.HOUR_OF_DAY, 0);
		hoje.set(Calendar.MINUTE, 0);
		hoje.set(Calendar.SECOND, 0);
		hoje.set(Calendar.MILLISECOND, 0);
		return hoje.getTime();
	}

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Date dataConvertida = null;

		if(value != null && !"".equals(value) ) {
			if("hoje".equals(value)) {
				dataConvertida = getDataHoje();
			} else {
				try {
					dataConvertida = formatador.parse(value);
				} catch (ParseException e) {
					FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Data incorreta",
							"Digite uma data no formato correto: dd/MM/yyyy ou hoje");
					throw new ConverterException(msg);
				}
			}
		}

		return dataConvertida;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object object) {
		if(getDataHoje().equals((Date) object)) {
			return "hoje";
		}
		return formatador.format((Date) object);
	}

}
