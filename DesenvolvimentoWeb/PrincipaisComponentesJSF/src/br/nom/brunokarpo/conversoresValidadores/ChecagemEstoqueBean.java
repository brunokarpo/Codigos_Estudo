package br.nom.brunokarpo.conversoresValidadores;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import br.nom.brunokarpo.primeirosComponentes.entidade.Item;

@ManagedBean
@ViewScoped
public class ChecagemEstoqueBean implements Serializable{

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private Item item = new Item();
	private List<Item> estoque = new ArrayList<Item>();

	public void incluir() {
		this.estoque.add(this.item);
		this.item = new Item();
	}

	public List<Item> getEstoque() {
		return this.estoque;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public Item getItem() {
		return this.item;
	}

}
