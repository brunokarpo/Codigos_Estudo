package br.nom.brunokarpo.conversoresValidadores.validator;

import java.util.Calendar;
import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator("nom.brunokarpo.DiaUtilValidator")
public class DiaUtilValidator implements Validator {

	@Override
	public void validate(FacesContext context, UIComponent component, Object object)
			throws ValidatorException {
		Calendar data = Calendar.getInstance();

		data.setTime((Date) object);
		int diaSemana = data.get(Calendar.DAY_OF_WEEK);

		if(diaSemana == Calendar.SATURDAY || diaSemana == Calendar.SUNDAY) {
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
					"Dia da semana inválido", "Digite um dia que compreenda de segunda à sexta");
			throw new ValidatorException(msg);
		}
	}

}
