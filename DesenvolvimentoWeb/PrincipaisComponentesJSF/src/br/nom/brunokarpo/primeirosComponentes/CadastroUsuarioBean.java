package br.nom.brunokarpo.primeirosComponentes;

import java.io.Serializable;
import java.util.Calendar;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

@ManagedBean
public class CadastroUsuarioBean implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private String nome;
	private String email;
	private String senha;

	public void cadastrar() {
		if(this.getNome() == null || this.getNome().length() < 10) {
			adicionarMensagem("frm:nome", FacesMessage.SEVERITY_ERROR,
					"Nome incompleto", "Informe seu nome completo");
		}

		if(ehDiaDeDescanso()) {
			adicionarMensagem(null, FacesMessage.SEVERITY_WARN,
					"Hoje é dia de descanso", "Não é possível adicionar usuário em dias de descanso");
		}

		if(!FacesContext.getCurrentInstance().getMessages().hasNext()) {
			// Lógica de cadastro
			// ...

			adicionarMensagem(null, FacesMessage.SEVERITY_INFO,
					"Cadastrado com sucesso", "Cliente cadastrado com sucesso no banco de dados");
		}
	}

	private void adicionarMensagem(String clientId, Severity severity, String sumary, String detail) {
		FacesContext context = FacesContext.getCurrentInstance();
		FacesMessage message = new FacesMessage(severity, sumary, detail);

		context.addMessage(clientId, message);
	}

	private boolean ehDiaDeDescanso() {
		int calendar = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
		return calendar == Calendar.SATURDAY || calendar == Calendar.SUNDAY;
	}

	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}

}
