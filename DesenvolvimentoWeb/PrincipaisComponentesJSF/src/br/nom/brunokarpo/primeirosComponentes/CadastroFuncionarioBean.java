package br.nom.brunokarpo.primeirosComponentes;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class CadastroFuncionarioBean implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private String sexo;
	private boolean cadastrado;
	private String[] linguagens;

	public String enviar() {
		return "Confirmacao";
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public boolean isCadastrado() {
		return cadastrado;
	}

	public void setCadastrado(boolean cadastrado) {
		this.cadastrado = cadastrado;
	}

	public String[] getLinguagens() {
		return linguagens;
	}

	public void setLinguagens(String[] linguagens) {
		this.linguagens = linguagens;
	}

}
