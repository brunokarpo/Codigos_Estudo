package br.nom.brunokarpo.primeirosComponentes.entidade;

import java.io.Serializable;

public class Produto implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private String nome;
	private String fabricante;
	private Integer quantidade;

	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getFabricante() {
		return fabricante;
	}
	public void setFabricante(String fabricante) {
		this.fabricante = fabricante;
	}
	public Integer getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}
}
