package br.nom.brunokarpo.primeirosComponentes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import br.nom.brunokarpo.primeirosComponentes.entidade.Produto;

@ManagedBean
@ApplicationScoped
public class CadastroProdutosBean implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private List<Produto> produtos;
	private Produto produto;

	@PostConstruct
	public void init() {
		this.produto = new Produto();
	}
	public void salvar() {
		this.produtos.add(this.produto);
		this.produto = new Produto();
	}

	public String obterAjuda() {
		if(this.produtos.isEmpty()) {
			return "ajuda";
		} else {
			return "ajudaTelefone";
		}

	}

	public List<Produto> getProdutos() {
		if(this.produtos == null) {
			this.produtos = new ArrayList<Produto>();
		}
		return this.produtos;
	}
	public Produto getProduto() {
		return this.produto;
	}


}
