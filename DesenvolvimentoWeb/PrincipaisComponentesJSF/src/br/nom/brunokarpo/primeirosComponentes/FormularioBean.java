package br.nom.brunokarpo.primeirosComponentes;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ActionEvent;

@ManagedBean
@SessionScoped
public class FormularioBean implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private String nome;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void resetarFormulario(ActionEvent event) {
		if(!"".equals(this.getNome())) {
			this.nome = null;
		}
	}


}
