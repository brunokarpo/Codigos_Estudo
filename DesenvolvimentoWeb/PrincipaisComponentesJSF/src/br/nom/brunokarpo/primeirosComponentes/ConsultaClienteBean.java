package br.nom.brunokarpo.primeirosComponentes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import br.nom.brunokarpo.primeirosComponentes.entidade.Cliente;

@ManagedBean
@ViewScoped
public class ConsultaClienteBean implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public List<Cliente> clientes = new ArrayList<Cliente>();

	public void consultar() {
		clientes.add(new Cliente(1, "Bruno Nogueira", "Goiânia"));
		clientes.add(new Cliente(2, "Ademir Nogueira", "Goiânia"));
		clientes.add(new Cliente(3, "Eliana Nogueira", "Luziânia"));
		clientes.add(new Cliente(4, "Aldemiro Araujo", "Goiânia"));
	}

	public void salvar() {
		for(Cliente cliente : this.getClientes()) {
			System.out.println(cliente.getCodigo() + " - " + cliente.getNome());
		}
	}

	public List<Cliente> getClientes() {
		return this.clientes;
	}
}
