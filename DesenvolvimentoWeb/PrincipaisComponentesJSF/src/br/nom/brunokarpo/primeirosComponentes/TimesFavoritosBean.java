package br.nom.brunokarpo.primeirosComponentes;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class TimesFavoritosBean implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private String[] times;

	public String[] getTimes() {
		return times;
	}

	public void setTimes(String[] times) {
		this.times = times;
	}

	public void escolher() {
		System.out.println("Times escolhidos");
		System.out.println("----------------");
		for(String time : getTimes()) {
			System.out.println(time);
		}
	}

}
