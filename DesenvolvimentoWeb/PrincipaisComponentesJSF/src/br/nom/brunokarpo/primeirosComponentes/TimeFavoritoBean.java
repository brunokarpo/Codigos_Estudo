package br.nom.brunokarpo.primeirosComponentes;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class TimeFavoritoBean implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private String time;

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public void escolher() {
		System.out.println("Time escolhido: " + time);
	}

}
