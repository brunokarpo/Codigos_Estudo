import static org.junit.Assert.assertEquals;

import java.util.List;

import org.hibernate.Session;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import br.nom.brunokarpo.dao.util.HibernateUtil;
import br.nom.brunokarpo.financeiro.entidades.Pessoa;


public class FuncionamentoHibernateTeste {

	private Session session;

	@BeforeClass
	public static void prepararBancoDeDadosParaExecucaoDeTestes() {
		Session s = HibernateUtil.getSession();

		s.beginTransaction();
		List<Pessoa> pessoas = s.createCriteria(Pessoa.class).list();
		for(Pessoa p : pessoas) {
			s.delete(p);
		}
		s.getTransaction().commit();
	}

	@Before
	public void criarConexao() {
		session = HibernateUtil.getSession();
	}

	@Test
	public void deveInserirUmaPessoa() {
		session.getTransaction().begin();
		session.persist(new Pessoa("Bruno Nogueira"));
		session.getTransaction().commit();
	}

	@Test
	public void deveInserirVariasPessoas() {
		session.getTransaction().begin();
		session.persist(new Pessoa("Ademir Nogueira"));
		session.persist(new Pessoa("Eliana Nogueira"));
		session.persist(new Pessoa("Aldemiro Araujo"));
		session.getTransaction().commit();
	}

	@Test
	public void deveBuscarTodasPessoasDoBanco() {
		List<Pessoa> lista = session.createCriteria(Pessoa.class).list();

		for(Pessoa p : lista) {
			System.out.println(p.getCodigo() + " - " + p.getNome());
		}

		assertEquals(4, lista.size());
	}

	@After
	public void fecharConexao() {
		session.close();
	}

}
