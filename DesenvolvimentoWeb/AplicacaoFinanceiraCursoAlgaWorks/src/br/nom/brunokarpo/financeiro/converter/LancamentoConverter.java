package br.nom.brunokarpo.financeiro.converter;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import br.nom.brunokarpo.financeiro.entidades.Lancamento;
import br.nom.brunokarpo.financeiro.managedBeans.FacesUtil;
import br.nom.brunokarpo.financeiro.repository.Lancamentos;
import br.nom.brunokarpo.financeiro.repository.Repositorios;

@FacesConverter(forClass=Lancamento.class)
public class LancamentoConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Lancamento lancamento = null;
		Lancamentos lancamentos = Repositorios.getRepositorios().getLancamentos();

		if(value != null && !"".equals(value)) {
			lancamento = lancamentos.porCodigo(new Integer(value));

			if(lancamento == null) {
				String message = "Não existe esse lançamento";
				FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
						message, message);
				throw new ConverterException(msg);
			}
		}

		return lancamento;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if(value != null) {
			Integer codigo = ((Lancamento) value).getCodigo();
			return codigo == null ? "" : codigo.toString();
		}
		return null;
	}

}
