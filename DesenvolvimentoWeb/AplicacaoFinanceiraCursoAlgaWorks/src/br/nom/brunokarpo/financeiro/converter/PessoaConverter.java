package br.nom.brunokarpo.financeiro.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import br.nom.brunokarpo.financeiro.entidades.Pessoa;
import br.nom.brunokarpo.financeiro.repository.Pessoas;
import br.nom.brunokarpo.financeiro.repository.Repositorios;

@FacesConverter(forClass=Pessoa.class)
public class PessoaConverter implements Converter{

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Pessoa retorno = null;

		if(value != null) {
			Pessoas pessoas = Repositorios.getRepositorios().getPessoas();
			retorno = pessoas.porCodigo(new Integer(value));
		}
		return retorno;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		Pessoa pessoa = (Pessoa) value;

		if(pessoa != null) {
			return pessoa.getCodigo().toString();
		}

		return null;
	}

}
