package br.nom.brunokarpo.financeiro.repository;

import java.util.List;

import br.nom.brunokarpo.financeiro.entidades.Pessoa;

public interface Pessoas {

	public List<Pessoa> todas();
	public Pessoa porCodigo(Integer codigo);

}
