package br.nom.brunokarpo.financeiro.repository;

import java.io.Serializable;

import org.hibernate.Session;

import br.nom.brunokarpo.financeiro.managedBeans.FacesUtil;
import br.nom.brunokarpo.financeiro.repository.impl.LancamentosHibernate;
import br.nom.brunokarpo.financeiro.repository.impl.PessoasHibernate;

public class Repositorios implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private static Repositorios repositorios;

	private Repositorios() {
		// Singleton marotao
	}

	public static Repositorios getRepositorios() {
		if(repositorios == null ) {
			repositorios = new Repositorios();
		}
		return repositorios;
	}

	public Pessoas getPessoas() {
		return new PessoasHibernate((Session) FacesUtil.getRequestAttribute("session"));
	}

	public Lancamentos getLancamentos() {
		return new LancamentosHibernate((Session) FacesUtil.getRequestAttribute("session"));
	}

}
