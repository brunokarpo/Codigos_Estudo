package br.nom.brunokarpo.financeiro.negocio;

public class RegraNegocioException extends Exception {

	public RegraNegocioException(String message) {
		super(message);
	}
}
