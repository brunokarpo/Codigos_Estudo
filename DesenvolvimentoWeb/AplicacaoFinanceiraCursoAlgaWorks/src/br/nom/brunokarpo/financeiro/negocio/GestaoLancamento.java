package br.nom.brunokarpo.financeiro.negocio;

import br.nom.brunokarpo.financeiro.entidades.Lancamento;
import br.nom.brunokarpo.financeiro.repository.Lancamentos;
import br.nom.brunokarpo.financeiro.repository.Repositorios;

public class GestaoLancamento {

	private Lancamentos lancamentos = Repositorios.getRepositorios().getLancamentos();

	public void salvarLancamento(Lancamento lancamento) throws RegraNegocioException {

		if(!existeLancamentoSemelhante(lancamento)) {
			lancamentos.guardar(lancamento);
		} else {
			throw new RegraNegocioException("Já existe outro lancamento semelhante cadastrado!");
		}

	}

	private boolean existeLancamentoSemelhante(Lancamento lancamento) {
		Lancamento lancamentoSemelhante = lancamentos.comDadosIguais(lancamento);
		return lancamentoSemelhante != null && !lancamentoSemelhante.equals(lancamento);
	}

	public void excluirLancamento(Lancamento lancamento) throws RegraNegocioException {
		if(lancamento.isPago()) {
			throw new RegraNegocioException("Lancamento já pago não pode ser excluído");
		} else {
			lancamentos.remover(lancamento);
		}
	}

}
