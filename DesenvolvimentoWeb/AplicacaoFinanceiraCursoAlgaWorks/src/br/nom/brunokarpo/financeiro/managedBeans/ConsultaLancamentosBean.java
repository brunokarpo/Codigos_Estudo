package br.nom.brunokarpo.financeiro.managedBeans;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import br.nom.brunokarpo.financeiro.entidades.Lancamento;
import br.nom.brunokarpo.financeiro.negocio.GestaoLancamento;
import br.nom.brunokarpo.financeiro.negocio.RegraNegocioException;
import br.nom.brunokarpo.financeiro.repository.Lancamentos;
import br.nom.brunokarpo.financeiro.repository.Repositorios;

@ManagedBean
public class ConsultaLancamentosBean implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private Lancamento lancamentoSelecionado;

	@SuppressWarnings("unchecked")
	public List<Lancamento> getLancamentos() {
		Lancamentos lancamentos = Repositorios.getRepositorios().getLancamentos();

		return lancamentos.todos();
	}

	public void excluir() {
		GestaoLancamento service = new GestaoLancamento();

		try {
			service.excluirLancamento(this.lancamentoSelecionado);

			FacesUtil.adicionarMensagem(FacesMessage.SEVERITY_INFO, "Lançamento excluído com sucesso");
			FacesContext.getCurrentInstance().renderResponse();
		} catch (RegraNegocioException e) {
			FacesUtil.adicionarMensagem(FacesMessage.SEVERITY_ERROR, e.getMessage());
		}

	}

	public Lancamento getLancamentoSelecionado() {
		return lancamentoSelecionado;
	}
	public void setLancamentoSelecionado(Lancamento lancamentoSelecionado) {
		this.lancamentoSelecionado = lancamentoSelecionado;
	}

}
