package br.nom.brunokarpo.financeiro.managedBeans;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

import br.nom.brunokarpo.financeiro.entidades.Lancamento;
import br.nom.brunokarpo.financeiro.entidades.Pessoa;
import br.nom.brunokarpo.financeiro.entidades.TipoLancamento;
import br.nom.brunokarpo.financeiro.negocio.GestaoLancamento;
import br.nom.brunokarpo.financeiro.negocio.RegraNegocioException;
import br.nom.brunokarpo.financeiro.repository.Lancamentos;
import br.nom.brunokarpo.financeiro.repository.Pessoas;
import br.nom.brunokarpo.financeiro.repository.Repositorios;

@ManagedBean
public class CadastroLancamentoBean implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private Lancamento lancamento;

	@PostConstruct
	public void iniciar() {
		this.lancamento = new Lancamento();
	}

	public TipoLancamento[] getTiposLancamentosPossiveis() {
		return TipoLancamento.values();
	}
	@SuppressWarnings("unchecked")
	public List<Pessoa> getPessoas() {
		Pessoas pessoas = Repositorios.getRepositorios().getPessoas();

		return pessoas.todas();
	}

	public void lancamentoPagoModificado(ValueChangeEvent event) {
		this.lancamento.setPago((Boolean) event.getNewValue());
		this.lancamento.setDataPagamento(null);
		FacesContext.getCurrentInstance().renderResponse();
	}

	public void salvar() {
		GestaoLancamento service = new GestaoLancamento();

		try {
			service.salvarLancamento(this.lancamento);

			FacesUtil.adicionarMensagem(FacesMessage.SEVERITY_INFO, "Lançamento salvo com sucesso");
			this.lancamento = new Lancamento();
		} catch (RegraNegocioException e) {
			FacesUtil.adicionarMensagem(FacesMessage.SEVERITY_ERROR, e.getMessage());
		}

	}

	public Lancamento getLancamento() {
		return this.lancamento;
	}

	public void setLancamento(Lancamento lancamento) throws CloneNotSupportedException {
		this.lancamento = lancamento;
		if(this.lancamento == null) {
			this.lancamento = new Lancamento();
		} else {
			this.lancamento = (Lancamento) lancamento.clone();
		}
	}

	public boolean isNovoLancamento() {
		if(this.lancamento.getCodigo() == null) {
			return true;
		}
		return false;
	}
}
