package br.nom.brunokarpo.financeiro.managedBeans;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

public class FacesUtil {

	public static void adicionarMensagem(Severity severity, String mensagem) {
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(severity,
						mensagem, mensagem));
	}

	public static Object getRequestAttribute(String name) {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ExternalContext external = facesContext.getExternalContext();
		HttpServletRequest request = (HttpServletRequest) external.getRequest();
		return request.getAttribute(name);
	}
}
