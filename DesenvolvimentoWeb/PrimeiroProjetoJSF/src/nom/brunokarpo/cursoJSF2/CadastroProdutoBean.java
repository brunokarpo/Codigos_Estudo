package nom.brunokarpo.cursoJSF2;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.NoneScoped;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import nom.brunokarpo.cursoJSF2.modelo.Produto;

@ManagedBean(name="cadastroProdutos")
//@ApplicationScoped
@SessionScoped
//@ViewScoped
//@RequestScoped
//@NoneScoped
public class CadastroProdutoBean implements Serializable{

	private List<Produto> produtos;
	private Produto produto;
	private String valorBotao;

	private List<Produto> produtosSelecionados;
	private Produto produtoSelecionado;
	private String nomePesquisa;

	public CadastroProdutoBean() {
		this.produtos = new ArrayList<Produto>();
		this.produtosSelecionados = new ArrayList<Produto>();
		this.produto = new Produto();
	}

	@PostConstruct
	public void inicializar() {
		System.out.println("Iniciando Managed bean");
	}

	@PreDestroy
	public void finalizar() {
		System.out.println("Finalizando Managed bean");
	}

	public String apertarBotao() {
		if(this.produtos.isEmpty()) {
			return "OlaMundo?faces-redirect=true";
		} else {
			return "Teste?faces-redirect=true";
		}
	}

	public void validarInclusao(ActionEvent event) {
		if("".equals( this.produto.getFabricante() ) ) {
			this.produto.setFabricante("Sem fabricante");
		}
	}

	public void excluir() {
		this.produtos.remove( this.produtoSelecionado );
	}

	public void nomePesquisaAlterado(ValueChangeEvent event) {
		this.produtosSelecionados.clear();

		for(Produto produto : this.produtos) {
			if(produto.getNome().toLowerCase().startsWith( event.getNewValue().toString().toLowerCase())) {
				this.produtosSelecionados.add(produto);
			}
		}
	}

	/**
	 * @return the produto
	 */
	public Produto getProduto() {
		return produto;
	}

	/**
	 * @return the produtos
	 */
	public List<Produto> getProdutos() {
		return produtos;
	}


	/**
	 * @return the valorBotao
	 */
	public String getValorBotao() {
		if(this.produtos.isEmpty()) {
			valorBotao = "Dê um oi para o mundo";
		} else {
			valorBotao = "Faça algum teste";
		}
		return valorBotao;
	}

	/**
	 * @return the produtoSelecionado
	 */
	public Produto getProdutoSelecionado() {
		return produtoSelecionado;
	}
	/**
	 * @param produtoSelecionado the produtoSelecionado to set
	 */
	public void setProdutoSelecionado(Produto produtoSelecionado) {
		this.produtoSelecionado = produtoSelecionado;
	}
	/**
	 * @return the nomePesquisa
	 */
	public String getNomePesquisa() {
		return nomePesquisa;
	}
	/**
	 * @param nomePesquisa the nomePesquisa to set
	 */
	public void setNomePesquisa(String nomePesquisa) {
		this.nomePesquisa = nomePesquisa;
	}
	/**
	 * @return the produtosSelecionados
	 */
	public List<Produto> getProdutosSelecionados() {
		return produtosSelecionados;
	}

	public void incluir() {
		this.produtos.add(this.produto);
		this.produto = new Produto();
	}

}
