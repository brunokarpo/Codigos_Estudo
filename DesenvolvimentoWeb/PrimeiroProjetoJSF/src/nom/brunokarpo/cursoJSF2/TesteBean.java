package nom.brunokarpo.cursoJSF2;

import javax.faces.bean.ManagedBean;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlInputText;

/**
 * @author bruno nogueira
 *
 * <p> Classe criada para testar as funcionalidades de um backing bean.
 *
 */
@ManagedBean(name="teste")
public class TesteBean {

	private HtmlInputText campo;
	private HtmlCommandButton botao;

	/**
	 * Esse metodo vai ser chamado ao clique de um botao e deve alterar os atributos do componente na tela.
	 */
	public void clicar() {
		if(this.campo.isDisabled()) {
			this.campo.setDisabled(false);
			this.botao.setStyle("");
		} else {
			this.campo.setDisabled(true);
			this.botao.setStyle("background-color: red; color: white");
		}
	}

	/**
	 * @return the campo
	 */
	public HtmlInputText getCampo() {
		return campo;
	}
	/**
	 * @param campo the campo to set
	 */
	public void setCampo(HtmlInputText campo) {
		this.campo = campo;
	}
	/**
	 * @return the botao
	 */
	public HtmlCommandButton getBotao() {
		return botao;
	}
	/**
	 * @param botao the botao to set
	 */
	public void setBotao(HtmlCommandButton botao) {
		this.botao = botao;
	}



}
