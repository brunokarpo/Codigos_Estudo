package nom.brunokarpo.cursoJSF2;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class OlaMundoBean {

	private String nome;

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}
	/**
	 * @param nome the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	public void enviar() {
		this.setNome(this.getNome().toUpperCase());
	}

}
