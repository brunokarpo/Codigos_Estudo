package nom.brunokarpo.sistemaPedidoVenda;

import java.util.ArrayList;
import java.util.List;

import nom.brunokarpo.sistemaPedidoVenda.desconto.CalculadoraFaixaDesconto;

public class Pedido {

	private List<ItemPedido> itens = new ArrayList<>();
	private CalculadoraFaixaDesconto calculadoraFaixaDesconto;

	public Pedido(CalculadoraFaixaDesconto calculadoraFaixaDesconto) {
		this.calculadoraFaixaDesconto = calculadoraFaixaDesconto;
	}

	public void adicionarItem(ItemPedido itemPedido) throws QuantidadeDeItensInvalidaException {
		validarQuantidadePedido(itemPedido);

		this.itens.add(itemPedido);
	}

	public ResumoPedido resumo() {
		double valorTotal = itens.stream().mapToDouble(i -> i.getValorUnitario() * i.getQuantidade()).sum();
		double desconto = calculadoraFaixaDesconto.desconto(valorTotal);

		return new ResumoPedido(valorTotal, desconto);
	}

	private void validarQuantidadePedido(ItemPedido itemPedido)	throws QuantidadeDeItensInvalidaException {
		if(itemPedido.getQuantidade() < 0)
			throw new QuantidadeDeItensInvalidaException();
	}
}
