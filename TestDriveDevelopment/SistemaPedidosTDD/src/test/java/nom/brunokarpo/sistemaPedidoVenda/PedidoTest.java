package nom.brunokarpo.sistemaPedidoVenda;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class PedidoTest {

	private PedidoBuilder pedido;

	@Before
	public void setUp() throws Exception {
		pedido = new PedidoBuilder();
	}

	@Test
	public void devePermitirAdicionarUmItemNoPedido() throws Exception {
		pedido.comItem(3.0, 10);
	}

	@Test
	public void deveCalcularValorTotalEDescontoParaPedidoVazio() throws Exception {
		assertResumoPedido(0.0, 0.0);
	}

	@Test
	public void deveCalcularOResumoParaUmItemSemDesconto() throws Exception {
		pedido.comItem(5.0, 5);
		assertResumoPedido(25.0, 0.0);
	}

	@Test
	public void deveCalcularResumoParaDoisItensSemDesconto() throws Exception {

		pedido.comItem(3.0, 3)
				.comItem(7.0, 3);

		assertResumoPedido(30.0, 0);
	}

	@Test
	public void deveAplicarDescontoNaPrimeiraFaixa() throws Exception {
		pedido.comItem(20.0, 20);

		assertResumoPedido(400, 16.0);
	}

	@Test
	public void deveAplicarDescontoNaSegundaFaixa() throws Exception {
		pedido.comItem(15.0, 30)
				.comItem(15.0, 30);

		assertResumoPedido(900, 54.0);
	}

	@Test
	public void deveAplicarDescontoNaTerceiraFaixa() throws Exception {
		pedido.comItem(15.0, 30)
				.comItem(15.0, 30)
				.comItem(10.0, 30);

		assertResumoPedido(1200, 96);
	}

	@Test(expected=QuantidadeDeItensInvalidaException.class)
	public void naoDeveAceitarPedidosComItensComQuantidadesNegativas() throws Exception {
		pedido.comItem(30.0, -10);
	}

	private void assertResumoPedido(double valorTotal, double desconto) {
		ResumoPedido resumoPedido = pedido.construir().resumo();

		assertEquals(new ResumoPedido(valorTotal, desconto), resumoPedido);
	}
}
