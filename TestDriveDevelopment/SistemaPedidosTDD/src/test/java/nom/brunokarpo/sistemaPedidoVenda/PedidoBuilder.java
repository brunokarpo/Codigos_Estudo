package nom.brunokarpo.sistemaPedidoVenda;

import nom.brunokarpo.sistemaPedidoVenda.desconto.CalculadoraDescontoPrimeiraFaixa;
import nom.brunokarpo.sistemaPedidoVenda.desconto.CalculadoraDescontoSegundaFaixa;
import nom.brunokarpo.sistemaPedidoVenda.desconto.CalculadoraDescontoTerceiraFaixa;
import nom.brunokarpo.sistemaPedidoVenda.desconto.CalculadoraFaixaDesconto;
import nom.brunokarpo.sistemaPedidoVenda.desconto.SemDesconto;

public class PedidoBuilder {

	private Pedido instancia;

	public PedidoBuilder() {
		CalculadoraFaixaDesconto calculadora =
				new CalculadoraDescontoTerceiraFaixa(
					new CalculadoraDescontoSegundaFaixa(
						new CalculadoraDescontoPrimeiraFaixa(
							new SemDesconto(null))));

		instancia = new Pedido(calculadora);
	}

	public PedidoBuilder comItem(double valorUnitario, int quantidade) throws QuantidadeDeItensInvalidaException {
		instancia.adicionarItem(new ItemPedido("Gerado", valorUnitario, quantidade));
		return this;
	}

	public Pedido construir() {
		return this.instancia;
	}
}
