import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


public class CamelCaseConverterTest {

	private CamelCaseConverter camelCase;

	@Before
	public void setUp() throws Exception {
		this.camelCase = new CamelCaseConverter();
	}

	@Test
	public void deveAplicarCamelCaseEmNomeUnico() throws Exception {
		assertEquals("Lionel", camelCase.converter("lionel"));
	}

	@Test
	public void deveConverterNomeSimplesMisturadoMaisculoEMinusculo() throws Exception {
		assertEquals("Lionel", camelCase.converter("lIOneL"));
	}

	@Test
	public void deveAplicarCamelCaseEmNomeComposto() throws Exception {
		assertEquals("Lionel Messi", camelCase.converter("lionel messi"));
	}

	@Test
	public void deveConverterNomeCompostoMisturadoMinusculoEMaiusculo() throws Exception {
		assertEquals("Lionel Messi", camelCase.converter("lIOneL MEsSI"));
	}

	@Test
	public void deveColocarConectivosDosNomesEmMinusculo() throws Exception {
		assertEquals("Bruno Nogueira de Oliveira", camelCase.converter("bruno nogueira DE oliveira"));
		assertEquals("João da Costa e Silva", camelCase.converter("JOÃO DA COSTA E SILVA"));
	}
}
