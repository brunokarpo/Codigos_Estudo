import java.util.ArrayList;
import java.util.List;


public class CamelCaseConverter {

	public String converter(String value) {
		String resultado = "";

		String temp = "";
		int i = 0;
		int posicaoPrimeiraLetra = i;
		while( i < value.length()) {
			if(" ".equals(value.substring(i, i+1)) || i+1 == value.length()) {
				temp = value.substring(posicaoPrimeiraLetra, i+1);
				if(isConectivo(temp)) {
					resultado += temp.toLowerCase();
				} else {
					resultado += converterPalavraUnica(temp);
				}
				posicaoPrimeiraLetra = i+1;
				temp = "";
			}
			i++;
		}

		return resultado;
	}

	private String converterPalavraUnica(String value) {
		boolean primeiraLetra = true;
		String resultado = "";

		for(int i = 0 ; i < value.length(); i++) {
			String letra = value.substring(i, i+1);

			if(!primeiraLetra) {
				resultado += letra.toLowerCase();
			} else {
				resultado += letra.toUpperCase();;
			}

			primeiraLetra = false;

			if(" ".equals(letra)) {
				primeiraLetra = true;
			}
		}

		return resultado;
	}

	private boolean isConectivo(String value) {
		if(listaDeConectivos().contains(value.toLowerCase().trim())) {
			return true;
		}
		return false;
	}

	private List<String> listaDeConectivos() {
		List<String> conectivos = new ArrayList<String>();

		conectivos.add("de");
		conectivos.add("da");
		conectivos.add("e");

		return conectivos;
	}

}
