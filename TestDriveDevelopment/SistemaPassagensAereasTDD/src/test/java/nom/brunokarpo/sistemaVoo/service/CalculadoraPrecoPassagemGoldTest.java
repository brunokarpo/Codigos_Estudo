package nom.brunokarpo.sistemaVoo.service;

import static org.junit.Assert.*;
import nom.brunokarpo.sistemaVoo.Voo;

import org.junit.Before;
import org.junit.Test;

public class CalculadoraPrecoPassagemGoldTest {

	private CalculadoraPrecoPassagem calculadora;
	@Before
	public void setUp() {
		calculadora = new CalculadoraPrecoPassagemGold();
	}

	@Test
	public void deveCalcularPrecoPassagemAbaixoLimite() throws Exception {
		assertValorPassagem(100.0, 90.0);
	}

	@Test
	public void deveCalcularPrecoPassagemAcimaLimite() throws Exception {
		assertValorPassagem(600.0, 510.0);
	}

	private void assertValorPassagem(double valorNormalVoo, double esperado ) {
		Voo voo = new Voo("São Paulo", "Rio de Janeiro", valorNormalVoo);
		double valor =  calculadora.calcular(voo);

		assertEquals(esperado, valor, 0.0001);
	}
}
