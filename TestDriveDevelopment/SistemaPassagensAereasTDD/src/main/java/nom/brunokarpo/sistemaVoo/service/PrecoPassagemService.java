package nom.brunokarpo.sistemaVoo.service;

import nom.brunokarpo.sistemaVoo.Passageiro;
import nom.brunokarpo.sistemaVoo.Voo;

public class PrecoPassagemService {

	public double calcular(Passageiro passageiro, Voo voo) {
		return passageiro.getTipo().getCalculadora().calcular(voo);
	}

}
