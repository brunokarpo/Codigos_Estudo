package nom.brunokarpo.sistemaVoo;

import nom.brunokarpo.sistemaVoo.service.CalculadoraPrecoPassagem;
import nom.brunokarpo.sistemaVoo.service.CalculadoraPrecoPassagemGold;
import nom.brunokarpo.sistemaVoo.service.CalculadoraPrecoPassagemSilver;

public enum TipoPassageiro {

	GOLD(new CalculadoraPrecoPassagemGold()),
	SILVER(new CalculadoraPrecoPassagemSilver());

	CalculadoraPrecoPassagem calculadora;

	TipoPassageiro(CalculadoraPrecoPassagem calculadora) {
		this.calculadora = calculadora;
	}

	public CalculadoraPrecoPassagem getCalculadora() {
		return calculadora;
	}

}
