package nom.brunokarpo.sistemaVoo.service;

import nom.brunokarpo.sistemaVoo.Voo;

public class CalculadoraPrecoPassagemGold implements CalculadoraPrecoPassagem {

	private static final int LIMITE_PRECO = 500;
	private static final double TAXA_ACIMA_LIMITE = 0.85;
	private static final double TAXA_ABAIXO_LIMITE = 0.9;

	@Override
	public double calcular(Voo voo) {
		if(voo.getPreco() < LIMITE_PRECO)
			return calculaPrecoPassagemAbaixoLimite(voo);
		return calculaPrecoPassagemAcimaLimite(voo);
	}

	private double calculaPrecoPassagemAcimaLimite(Voo voo) {
		return voo.getPreco() * TAXA_ACIMA_LIMITE;
	}

	private double calculaPrecoPassagemAbaixoLimite(Voo voo) {
		return voo.getPreco() * TAXA_ABAIXO_LIMITE;
	}

}
