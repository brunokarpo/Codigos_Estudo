package nom.brunokarpo.sistemaVoo.service;

import nom.brunokarpo.sistemaVoo.Voo;

public interface CalculadoraPrecoPassagem {

	public double calcular(Voo voo);
}
