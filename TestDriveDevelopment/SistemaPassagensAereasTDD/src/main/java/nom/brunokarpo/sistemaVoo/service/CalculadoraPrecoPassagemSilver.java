package nom.brunokarpo.sistemaVoo.service;

import nom.brunokarpo.sistemaVoo.Voo;

public class CalculadoraPrecoPassagemSilver implements CalculadoraPrecoPassagem {

	private static final int LIMITE_PRECO = 700;
	private static final double TAXA_ACIMA_LIMITE = 0.90;
	private static final double TAXA_ABAIXO_LIMITE = 0.94;

	@Override
	public double calcular(Voo voo) {
		if(voo.getPreco() < LIMITE_PRECO)
			return calculaPrecoAbaixoLimite(voo);
		return calculaPrecoAcimaLimite(voo);
	}

	private double calculaPrecoAcimaLimite(Voo voo) {
		return voo.getPreco() * TAXA_ACIMA_LIMITE;
	}

	private double calculaPrecoAbaixoLimite(Voo voo) {
		return voo.getPreco() * TAXA_ABAIXO_LIMITE;
	}

}
