package nom.brunokarpo.sistemaPedidoMock.negocio;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import nom.brunokarpo.sistemaPedidoMock.model.Pedido;
import nom.brunokarpo.sistemaPedidoMock.model.StatusPedido;
import nom.brunokarpo.sistemaPedidoMock.model.builders.PedidoBuilder;
import nom.brunokarpo.sistemaPedidoMock.notificadores.Notificador;
import nom.brunokarpo.sistemaPedidoMock.notificadores.NotificadorEmail;
import nom.brunokarpo.sistemaPedidoMock.notificadores.NotificadorSms;
import nom.brunokarpo.sistemaPedidoMock.repositorio.Pedidos;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class PedidoServiceTest {

	private PedidoService pedidoService;
	private Pedido pedido;

	@Mock
	private Pedidos pedidos;

	@Mock
	private NotificadorEmail notificadoEmail;

	@Mock
	private NotificadorSms notificadorSms;

	@Before
	public void setup() {
		pedido = new PedidoBuilder()
						.comValor(100.0)
						.para("Joao", "joao@email.com", "9999-9999")
						.construir();

		MockitoAnnotations.initMocks(this);
		List<Notificador> notificadores = Arrays.asList(notificadoEmail, notificadorSms);

		pedidoService = new PedidoService(pedidos, notificadores);
	}

	@Test
	public void deveCalcularOImposto() throws Exception {
		double imposto = pedidoService.lancar(pedido);
		assertEquals(10.0, imposto, 0.0001);
	}

	@Test
	public void deveSalvarPedidoNoBancoDeDados() throws Exception {
		double imposto = pedidoService.lancar(pedido);

		Mockito.verify(pedidos).guardar(pedido);
	}

	@Test
	public void deveNotificarPorEmail() throws Exception {
		pedidoService.lancar(pedido);

		Mockito.verify(notificadoEmail).notificar(pedido);
	}

	@Test
	public void deveNotificarPorSms() throws Exception {
		pedidoService.lancar(pedido);

		Mockito.verify(notificadorSms).notificar(pedido);
	}

	@Test
	public void devePermitirPagamentoDePedidoPendente() throws Exception {
		Long codigo = 135L;
		StatusPedido pendente = StatusPedido.PENDENTE;
		configuraObjetoMockadoParaTeste(pendente, codigo);

		Pedido pedidoPago = pedidoService.pagar(codigo);

		assertEquals(StatusPedido.PAGO, pedidoPago.getStatus());
	}

	@Test(expected=StatusPedidoInvalidoException.class)
	public void naoDevePermitirPagamentoDePedidoJaPago() throws Exception {
		StatusPedido pago = StatusPedido.PAGO;
		Long codigo = 135L;
		configuraObjetoMockadoParaTeste(pago, codigo);

		pedidoService.pagar(codigo);
	}

	private void configuraObjetoMockadoParaTeste(StatusPedido status, Long codigo) {
		Pedido pedidoMockado = new Pedido();
		pedidoMockado.setStatus(status);
		Mockito.when(pedidos.buscarPeloCodigo(codigo)).thenReturn(pedidoMockado);
	}

}
