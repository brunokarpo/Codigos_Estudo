package nom.brunokarpo.sistemaPedidoMock.repositorio;

import nom.brunokarpo.sistemaPedidoMock.model.Pedido;

public class Pedidos {

	public void guardar(Pedido pedido) {
		System.out.println("Salvando pedido no banco de dados...");
	}

	public Pedido buscarPeloCodigo(Long codigo) {
		// Deveria ir no banco de dados buscar o pedido
		return new Pedido();
	}

}
