package nom.brunokarpo.sistemaPedidoMock.notificadores;

import nom.brunokarpo.sistemaPedidoMock.model.Pedido;

public class NotificadorSms implements Notificador {

	@Override
	public void notificar(Pedido pedido) {
		System.out.println("Enviando notificação por SMS...");
	}
}
