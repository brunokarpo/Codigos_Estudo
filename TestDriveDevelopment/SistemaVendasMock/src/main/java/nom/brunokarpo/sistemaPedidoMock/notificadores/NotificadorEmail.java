package nom.brunokarpo.sistemaPedidoMock.notificadores;

import nom.brunokarpo.sistemaPedidoMock.model.Pedido;

public class NotificadorEmail implements Notificador{

	@Override
	public void notificar(Pedido pedido) {
		System.out.println("Enviando email para o cliente...");
	}

}
