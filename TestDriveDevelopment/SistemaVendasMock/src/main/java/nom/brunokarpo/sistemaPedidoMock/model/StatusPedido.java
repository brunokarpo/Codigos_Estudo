package nom.brunokarpo.sistemaPedidoMock.model;

public enum StatusPedido {

	PENDENTE,
	PAGO;
}
