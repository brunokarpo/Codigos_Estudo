package nom.brunokarpo.sistemaPedidoMock.notificadores;

import nom.brunokarpo.sistemaPedidoMock.model.Pedido;

public interface Notificador {

	public void notificar(Pedido pedido) ;

}
