package nom.brunokarpo.sistemaPedidoMock.negocio;

import java.util.List;

import nom.brunokarpo.sistemaPedidoMock.model.Pedido;
import nom.brunokarpo.sistemaPedidoMock.model.StatusPedido;
import nom.brunokarpo.sistemaPedidoMock.notificadores.Notificador;
import nom.brunokarpo.sistemaPedidoMock.repositorio.Pedidos;

public class PedidoService {

	private Pedidos pedidos;
	private List<Notificador> notificadores;

	public PedidoService(Pedidos pedidos, List<Notificador> notificadores) {
		this.pedidos = pedidos;
		this.notificadores = notificadores;
	}

	public double lancar(Pedido pedido) {
		double imposto = pedido.getValor() * 0.1;

		pedidos.guardar(pedido);

		notificadores.forEach(a -> a.notificar(pedido));

		return imposto;
	}

	public Pedido pagar(Long codigo) {
		Pedido pedido = pedidos.buscarPeloCodigo(codigo);

		if(!pedido.getStatus().equals(StatusPedido.PENDENTE)) {
			throw new StatusPedidoInvalidoException();
		}

		pedido.setStatus(StatusPedido.PAGO);
		return pedido;
	}

}
